package page_processer

import (
	"dense_spider/core/common/extract"
	"dense_spider/core/common/page"
)

type PageProcesser interface {
	Process(p *page.Page, extractRules []extract.ExtractRule, maxCrawDepth uint)
}
