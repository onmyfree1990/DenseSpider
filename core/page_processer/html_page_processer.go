package page_processer

import (
	"bytes"
	"dense_spider/core/common/extract"
	"strings"

	"dense_spider/core/common/page"
	"dense_spider/core/common/util"

	"dense_spider/tools/xpath"
)

type HtmlPageProcesser struct {
}

func NewHtmlPageProcesser() *HtmlPageProcesser {
	return &HtmlPageProcesser{}
}
func (this *HtmlPageProcesser) Process(p *page.Page, extractRules []extract.ExtractRule, maxCrawDepth uint) {
	if !p.IsSucc() {
		println(p.Errormsg())
		return
	}

	body := p.GetBodyStr()
	if body == "" {
		return
	}
	node, err := xpath.ParseHTML(bytes.NewBuffer([]byte(body)))
	if err != nil {

		panic(err)
	}
	var pDepth = p.GetRequest().GetDepth() + 1

	if pDepth <= maxCrawDepth {
		linksXpath, err := xpath.Compile("//a/@href")
		if err != nil {

			panic(err)
		}
		links := linksXpath.Iter(node)

		for links.Next() {
			link := links.Node().String()

			normalizeUrl := util.NormalizeUrl(link, p.GetRequest().GetUrl())

			if normalizeUrl == "" {
				continue
			}
			p.AddTargetRequest(normalizeUrl, "html", pDepth)

			body = strings.Replace(body, link, normalizeUrl, -1)
		}
		linksXpath, err = xpath.Compile("//img/@src")
		if err != nil {

			panic(err)
		}
		links = linksXpath.Iter(node)

		for links.Next() {
			link := links.Node().String()

			normalizeUrl := util.NormalizeUrl(link, p.GetRequest().GetUrl())

			if normalizeUrl == "" {
				continue
			}
			p.AddTargetRequest(normalizeUrl, "html", pDepth)
			body = strings.Replace(body, link, normalizeUrl, -1)
		}
	}
	if extractRules != nil && len(extractRules) > 0 {
		for i := 0; i < len(extractRules); i++ {
			extractRule := extractRules[i]

			xpathRule, err := xpath.Compile(extractRule.GetXpath())
			if err != nil {
				panic(err)
			}
			xpathValue, ok := xpathRule.String(node)
			if ok {
				p.AddField(extractRule.GetName(), xpathValue)
			}

		}
	}
	//1. 抽取链接
	//2. 遍历链接，补全链接
	//3. 链接加入请求

	//4. 抽取指定规则的内容

}
