// pipeline_mongodb.go

package pipeline

import (
	"net/url"
	"time"

	"dense_spider/core/common/com_interfaces"
	"dense_spider/core/common/page_items"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type MDocument struct {
	Id bson.ObjectId "_id"

	Title        string
	Domain       string
	Content      string
	Url          string
	Created      time.Time
	DocPublished time.Time
}
type PipelineMongodb struct {
}

func NewPipelineMongodb() *PipelineMongodb {
	return &PipelineMongodb{}
}

func (this *PipelineMongodb) Process(items *page_items.PageItems, t com_interfaces.Task) {
	domain := ""
	url, err := url.Parse(items.GetRequest().GetUrl())
	if err != nil {
		domain = ""
	} else {
		domain = url.Host
	}

	content, ok := items.GetItem("Content")
	if !ok {
		content = ""
	}
	docPublished := time.Now()

	title, ok := items.GetItem("Title")
	if !ok {
		title = ""
	}
	if title == "" || content == "" {
		return
	}
	doc := &MDocument{Id: bson.NewObjectId(), Domain: domain, Url: items.GetRequest().GetUrl(), DocPublished: docPublished, Title: title, Content: content, Created: time.Now()}
	// 连接数据库
	session, err := mgo.Dial("127.0.0.1")
	if err != nil {
		panic(err)
	}
	defer session.Close()

	// 获取数据库,获取集合
	c := session.DB("go_spider").C("Document")

	// 存储数据

	err = c.Insert(doc)

	if err != nil {
		println(err.Error())
		panic(err)
	}
}
func main() {

}
